
$(document).scroll(function(){
    $('.navbar').scrollTop()
    {
        $('.navbar').css({"padding-top":"8px"});
        $('.navbar').css({"padding-bottom":"8px"});
        $('.navbar').css({"background-color":"#49473f5c"});
        $('.navbar').css({"transition":".5s"});
        $('.logo-cube').css({"transition":".5s"});
        $('.logo-cube').css({"opacity":"0.5"});
        $('.logo-cube').css({"transform":"scale(0.5)"});
        $('.logo-cube').css({"opacity":"0.3"});
        $('.logo-cube').css({"transform":"scale(0)"});
        // $('.logo-cube').css({"display":"none"});
        // $('.navbar-nav-link.selected.changed:after').css({"top":"36px"});
        $('.navbar-nav-link.selected').addClass('changed');
        $('.navbar-nav-link').addClass('hovered');



    }
});

$(window).scroll(function() {
    let scroll = $(window).scrollTop();
    if(!scroll) {
        $('.navbar').css({"padding-top":"26px", "padding-bottom":"26px", "transition":".2s"});
        $('.logo-cube').css({"transition":"all 0.2s ease 0.1s"});
        $('.logo-cube').css({"opacity":"0.7"});
        $('.logo-cube').css({"transform":"scale(0.7)"});
        $('.logo-cube').css({"opacity":"1"});
        $('.logo-cube').css({"transform":"scale(1)"});
        $('.navbar-nav-link.selected').removeClass('changed');
        $('.navbar-nav-link').removeClass('hovered');
    }
});



$(document).ready(function () {
    // $('.tabs__caption li').eq(2).addClass('active');
    $('.services-grid-container .services-items').click(function () {
        switchTabs(this, 'active', 'services-content');
    });

    function switchTabs(elem, switchClass, contentClass) {
        $(elem).addClass(switchClass).siblings().removeClass(switchClass);
        // console.log($(this).index())
        const tabsIndex = $(elem).index();
        $(`.${contentClass}`).removeClass(switchClass).eq(tabsIndex).addClass(switchClass);
    }

});




$(document).ready(function() {

    $(".our-work-images").hide();
    $(".our-work-images").slice(0,12).show();
	
  $('.btn-load-more').on('click', function(e) {
    e.preventDefault();
    $(e.currentTarget).attr('disabled', true);
    $('.loader').toggleClass('hide');

    setTimeout(function() {
      $('.loader').toggleClass('hide');
      $(e.currentTarget).attr('disabled', false);

    }, 700)
  });
});
	
    

    $(".btn-load-more").click(function(event){

        event.preventDefault();
        const imageType = $(".our-work-items.active").data("filter");
        setTimeout(function() {
        $(`.our-work-images${imageType}:hidden`).slice(0,6).show();
    }, 800)

    });



    $('.our-work-items').click(function() {
        $(this).addClass('active').siblings().removeClass("active");
        const imageType = $(this).data("filter");
        $(".our-work-images").hide();

        $(`.our-work-images${imageType}`).slice(0,6).show();

    });

	
	


	

  
  
// With Jquerry-->

   $('.our-work-images.on-hover').mouseover(function () {
       showToolTip(this, 'here')
   });

$('.our-work-images.on-hover').mouseout(function () {
   hideToolTip(this, 'here')
});


   function showToolTip(elem, className) {
       $(elem).addClass(className);
	   
	   
	   //append(`<span class="${className}">${$(elem).data('words')}</span>`)
   }

   function hideToolTip(elem, className) {
   $(elem).removeClass(className);
   
   //find(`.${className}`).remove();
   }



$('.slider-fb').hide();
$('.slider-fb.first:hidden').show();
$('.person').hide();
$('.person.first:hidden').show();
$('.logo').hide();
$('.logo.first:hidden').show();

$('.little-logo').click(switchPers);

function switchPers() {
    $('.little-logo').removeClass('little-logo-active');
    $(this).addClass('little-logo-active');
    let type = $(this).data('type');
    $('.slider-fb').hide();
    $(`.slider-fb.${type}:hidden`).show();
    $('.person').hide();
    $(`.person.${type}:hidden`).show()
    $('.logo').hide();
    $(`.logo.${type}:hidden`).show()
}

$('.right-arrow').click(function () {
    if ($('.little-logo.little-logo-active').next().hasClass("little-logo")) {
        let type = $('.little-logo.little-logo-active').next('.little-logo').data('type');
        $('.little-logo').removeClass('little-logo-active');
        $('.slider-fb').hide();
        $(`.slider-fb.${type}:hidden`).show();
        $('.person').hide();
        $(`.person.${type}:hidden`).show()
        $('.logo').hide();
        $(`.logo.${type}:hidden`).show()
        let elem = $(`[data-type = ${type}`);
        $(elem).addClass('little-logo-active');
    }
    else {
        $('.little-logo').removeClass('little-logo-active');
        $('.little-logo').first().addClass("little-logo-active");
        let type = $('.little-logo.little-logo-active').data('type');
        $('.slider-fb').hide();
        $(`.slider-fb.${type}:hidden`).show();
        $('.person').hide();
        $(`.person.${type}:hidden`).show()
        $('.logo').hide();
        $(`.logo.${type}:hidden`).show()
    }
});

$('.left-arrow').click(function () {
    if ($('.little-logo.little-logo-active').prev().hasClass("little-logo")) {
        let type = $('.little-logo.little-logo-active').prev('.little-logo').data('type');
        $('.little-logo').removeClass('little-logo-active');
        $('.slider-fb').hide();
        $(`.slider-fb.${type}:hidden`).show();
        $('.person').hide();
        $(`.person.${type}:hidden`).show()
        $('.logo').hide();
        $(`.logo.${type}:hidden`).show()
        let elem = $(`[data-type = ${type}`);
        $(elem).addClass('little-logo-active');
    }
    else {
        $('.little-logo').removeClass('little-logo-active');
        $('.little-logo').last().addClass("little-logo-active");
        let type = $('.little-logo.little-logo-active').data('type');
        $('.slider-fb').hide();
        $(`.slider-fb.${type}:hidden`).show();
        $('.person').hide();
        $(`.person.${type}:hidden`).show()
        $('.logo').hide();
        $(`.logo.${type}:hidden`).show()
    }
});


